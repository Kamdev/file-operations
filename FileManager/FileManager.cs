﻿using System;
using System.IO;
using System.Linq;

namespace FileManager
{
    public static class FileManager
    {
        /// <summary>
        /// Byte copies data from file to other file
        /// </summary>
        /// <param name="sourceFileName">File to read.</param>
        /// <param name="finalFileName">File to copy.</param>
        /// <returns>Count of bytes write into the <param name="finalFileName"></param>>.</returns>
        /// <exception cref="ArgumentNullException">Throws, when source or destination file name is
        /// null or empty.</exception>
        public static int CopyToFileUsingFileStream(string sourceFileName, string finalFileName)
        {
            if (string.IsNullOrEmpty(sourceFileName))
            {
                throw new ArgumentNullException(nameof(sourceFileName));
            }
            
            if (string.IsNullOrEmpty(finalFileName))
            {
                throw new ArgumentNullException(nameof(finalFileName));
            }

            int count = 0;
            
            using (FileStream sourceStream = new FileStream(sourceFileName, FileMode.Open))
            using (FileStream destinationStream = new FileStream(finalFileName, FileMode.OpenOrCreate))
            {
                while (count < sourceStream.Length)
                {
                    int readByte = sourceStream.ReadByte();
                    destinationStream.WriteByte((byte)readByte);
                    count++;
                }
                
            }
            
            return count;
        }

        /// <summary>
        /// Copies data in bytes format from text file to another one using MemoryStream
        /// </summary>
        /// <param name="sourceFileName">File to read.</param>
        /// <param name="finalFileName">File to copy.</param>
        /// <returns>Count of bytes write into the <param name="finalFileName"></param>>.</returns>
        /// <exception cref="ArgumentNullException">Throws, when source or destination file name is
        /// null or empty.</exception>
        public static int CopyToFileUsingMemoryStream(string sourceFileName, string finalFileName)
        {
            if (string.IsNullOrEmpty(sourceFileName))
            {
                throw new ArgumentNullException(nameof(sourceFileName));
            }
            
            if (string.IsNullOrEmpty(finalFileName))
            {
                throw new ArgumentNullException(nameof(finalFileName));
            }

            int count = 0;

            using (MemoryStream buffer = new MemoryStream())
            {
                using StreamReader reader = new StreamReader(sourceFileName);
                {
                    reader.BaseStream.CopyTo(buffer);
                }
                
                var bytes = buffer.ToArray();

                using (FileStream writer = new FileStream(finalFileName, FileMode.OpenOrCreate))
                {
                    foreach (var dataByte in bytes)
                    {
                        writer.WriteByte(dataByte);
                        count++;
                    }
                }
            }

            return count;
        }

        /// <summary>
        /// Copies data in bytes format from text file to another one
        /// </summary>
        /// <param name="sourceFileName">File to read.</param>
        /// <param name="finalFileName">File to copy.</param>
        /// <returns>Count of bytes write into the <param name="finalFileName"></param>>.</returns>
        /// <exception cref="ArgumentNullException">Throws, when source or destination file name is
        /// null or empty.</exception>
        public static int CopyToFileUsingBuffering(string sourceFileName, string finalFileName)
        {
            if (string.IsNullOrEmpty(sourceFileName))
            {
                throw new ArgumentNullException(nameof(sourceFileName));
            }
            
            if (string.IsNullOrEmpty(finalFileName))
            {
                throw new ArgumentNullException(nameof(finalFileName));
            }

            int countOfRecordedBytes = 0;
            
            using (FileStream sourceStream = new FileStream(sourceFileName, FileMode.Open))
            using (FileStream destinationStream = new FileStream(finalFileName, FileMode.OpenOrCreate))
            {
                sourceStream.CopyTo(destinationStream);
                countOfRecordedBytes = (int)destinationStream.Length;
            }

            return countOfRecordedBytes;
        }
        
        /// <summary>
        /// Copies data from text file to another one through buffer
        /// </summary>
        /// <param name="sourceFileName">File to read.</param>
        /// <param name="finalFileName">File to copy.</param>
        /// <returns>Count of bytes write into the <param name="finalFileName"></param>>.</returns>
        /// <exception cref="ArgumentNullException">Throws, when source or destination file name is
        /// null or empty.</exception>
        public static int CopyToFileUsingBufferStream(string sourceFileName, string finalFileName)
        {
            if (string.IsNullOrEmpty(sourceFileName))
            {
                throw new ArgumentNullException(nameof(sourceFileName));
            }
            
            if (string.IsNullOrEmpty(finalFileName))
            {
                throw new ArgumentNullException(nameof(finalFileName));
            }

            int byteCount = 0;
            
            using FileStream sourceStream = new FileStream(sourceFileName, FileMode.Open);
            using (BufferedStream buffer = new BufferedStream(sourceStream))
            {
                using (FileStream destinationStream = new FileStream(finalFileName, FileMode.OpenOrCreate))
                {
                    if (buffer.CanWrite)
                    {
                        buffer.CopyTo(destinationStream);
                    }

                    byteCount = (int)destinationStream.Length;
                }
            }

            return byteCount;
        }
        
        /// <summary>
        /// Copies data format from text file to another one in line format.
        /// </summary>
        /// <param name="sourceFileName">File to read.</param>
        /// <param name="finalFileName">File to copy.</param>
        /// <returns>Count of bytes write into the <param name="finalFileName"></param>>.</returns>
        /// <exception cref="ArgumentNullException">Throws, when source or destination file name is
        /// null or empty.</exception>
        public static int LineCopy(string sourceFileName, string finalFileName)
        {
            if (string.IsNullOrEmpty(sourceFileName))
            {
                throw new ArgumentNullException(nameof(sourceFileName));
            }
            
            if (string.IsNullOrEmpty(finalFileName))
            {
                throw new ArgumentNullException(nameof(finalFileName));
            }
            
            int linesCount = 0;
            
            using (StreamReader reader = new StreamReader(sourceFileName))
            using (StreamWriter writer = new StreamWriter(finalFileName))
            {
                string textLine;
                while ((textLine = reader.ReadLine()) is not null)
                {
                    writer.WriteLine(textLine);
                    linesCount++;
                }
            }

            return linesCount;
        }

        /// <summary>
        /// Compares data in two files.
        /// </summary>
        /// <param name="sourceFilePath">First file.</param>
        /// <param name="destinationFilePath">File with copied information.</param>
        /// <returns>True, if the data in both files equals, otherwise, false.</returns>
        /// <exception cref="ArgumentNullException">Throws, when source or destination file name is
        /// null or empty.</exception>
        public static bool CompareData(string sourceFilePath, string destinationFilePath)
        {
            if (string.IsNullOrEmpty(sourceFilePath))
            {
                throw new ArgumentNullException(nameof(sourceFilePath));
            }
            
            if (string.IsNullOrEmpty(destinationFilePath))
            {
                throw new ArgumentNullException(nameof(destinationFilePath));
            }

            bool equals = false;

            using StreamReader source = new StreamReader(sourceFilePath);
            using (StreamReader destination = new StreamReader(destinationFilePath))
            {
                string sourceFileText = source.ReadToEnd();
                string destinationFileText = destination.ReadToEnd();
                equals = sourceFileText == destinationFileText;
            }

            return equals;
        }
    }
}